from django.urls import path
from .views import donation_list, get_donation_param_doku, notify_doku
from .views import get_donation_param_midtrans, notify_midtrans

app_name = "payment_gateway"

urlpatterns = [
	path('donation-list.abs', donation_list),
	path('donation-param-doku.abs', get_donation_param_doku),
	path('notify-doku.abs', notify_doku),
	path('donation-param-midtrans.abs', get_donation_param_midtrans),
	path('notify-midtrans.abs', notify_midtrans)
]
