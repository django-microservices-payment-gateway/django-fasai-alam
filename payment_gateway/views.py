import requests
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .services import get_all_donations, generate_donation_param_doku, handle_notify_doku
from .services import generate_donation_param_midtrans, handle_notify_midtrans

# Create your views here.
def donation_list(request):
	return JsonResponse(get_all_donations(), safe=False)

@csrf_exempt
def get_donation_param_doku(request):
	return JsonResponse(generate_donation_param_doku(request))

@csrf_exempt
def notify_doku(request):
	return JsonResponse(handle_notify_doku(request))
	
@csrf_exempt
def get_donation_param_midtrans(request):
	return JsonResponse(generate_donation_param_midtrans(request))

@csrf_exempt
def notify_midtrans(request):
	return JsonResponse(handle_notify_midtrans(request))