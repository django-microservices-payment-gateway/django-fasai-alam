import datetime
import midtransclient
import requests
import json
import uuid
from hashlib import sha1
from .params import get_doku_donation_param_api, get_midtrans_donation_param_api
from .models import Donation, Income
from django.core import serializers

def get_all_donations():
	donation_objects = list()
	donations = Donation.objects.all()
	for donation in donations:
		donation_objects.append({
			'id': donation.pk,
			'name': donation.name,
			'email': donation.email,
			'phone': donation.phone,
			'amount': donation.amount,
			'paymentgateway': donation.paymentgateway,
			'requestdatetime': donation.requestdatetime,
			'trxstatus': donation.trxstatus,
			'transidmerchant': donation.transidmerchant,
			'orderid': donation.orderid,
			'words': donation.words,
			'incomeid': donation.incomeid.pk if donation.incomeid else 0
		})
	return {"data": donation_objects}

def generate_donation_param_doku(request):
	if request.method != "POST": return non_post_msg()
	
	name = request.POST["name"]
	email = request.POST["email"]
	phone = request.POST["phone"]
	amount = request.POST["amount"]
	
	env_doku = get_doku_donation_param_api()
	
	shared_key = env_doku["SHARED_KEY"]
	mallid = env_doku["MALLID"]
	doku_endpoint = env_doku["ENDPOINT"]
	
	timestamp = datetime.datetime.now().strftime("%Y.%m.%d.%H.%M.%S.%f")
	hashed_timestamp = sha1(timestamp.encode()).hexdigest()
	min_timestamp = hashed_timestamp[:10]
	dateonly = datetime.datetime.now().strftime("%Y%m%d")
	transaction_id = "AISCO-" + dateonly + "-" + min_timestamp
	session_id = transaction_id
	total_cost = amount
	total_cost_str = str(amount) + ".00"
	word_to_hash = total_cost_str+mallid+shared_key+transaction_id
	words = sha1(word_to_hash.encode()).hexdigest()
	date_time = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
	basket_str = "Donation,"+total_cost_str+",1,"+total_cost_str
	
	Donation.objects.create(name=name, email=email, phone=phone, amount=total_cost, 
	requestdatetime=date_time, mallid=mallid, transidmerchant=transaction_id, 
	words=words, sessionid=session_id, basket=basket_str, currency="360", 
	purchasecurrency="360", chainmerchant="NA", paymentgateway="DOKU", trxstatus="PENDING")
	
	result = {
		"ENDPOINT": doku_endpoint,
		"MALLID": mallid,
		"CHAINMERCHANT": "NA",
		"AMOUNT": total_cost_str,
		"PURCHASEAMOUNT": total_cost_str,
		"TRANSIDMERCHANT": transaction_id,
		"WORDS": words,
		"REQUESTDATETIME": date_time,
		"CURRENCY": "360",
		"PURCHASECURRENCY": "360",
		"SESSIONID": "session_id",
		"NAME": name,
		"EMAIL": email,
		"BASKET": basket_str,
		"MOBILEPHONE": phone
	}
	
	data_result = {"data": result}
	return data_result

def handle_notify_doku(request):
	if request.method != "POST": return non_post_msg()
	
	incoming_words = request.POST["WORDS"]
	amount = request.POST["AMOUNT"]
	transidmerchant = request.POST["TRANSIDMERCHANT"]
	approvalcode = request.POST["APPROVALCODE"]
	trxstatus = request.POST["RESULTMSG"]
	verifystatus = request.POST["VERIFYSTATUS"]
	
	print(transidmerchant)
	
	env_doku = get_doku_donation_param_api()
	
	shared_key = env_doku["SHARED_KEY"]
	mallid = env_doku["MALLID"]
	
	word_to_hash = amount+mallid+shared_key+transidmerchant+trxstatus+verifystatus
	calculated_words = sha1(word_to_hash.encode()).hexdigest()
	
	result_message = ""
	debug = True
	if incoming_words == calculated_words or debug:
		donation = Donation.objects.get(transidmerchant=transidmerchant)
		donation.trxstatus = trxstatus
		if trxstatus == "SUCCESS":
			name = donation.name
			description = "Donation from " + name + " with Payment Gateway DOKU"
			date_time = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
			
			income = Income.objects.create(datestamp=date_time, description=description, amount=int(amount))
			
			donation.incomeid = income
		donation.save()
		result_message = trxstatus
	else:
		result_message = "Invalid request"
		
	return {"data": result_message}

def generate_donation_param_midtrans(request):
	if request.method != "POST": return non_post_msg()
	
	name = request.POST["name"]
	email = request.POST["email"]
	phone = request.POST["phone"]
	amount = request.POST["amount"]
	
	midtrans_env = get_midtrans_donation_param_api()
	merchantid = midtrans_env["MerchantID"]
	clientkey = midtrans_env["ClientKey"]
	serverkey = midtrans_env["ServerKey"]
	
	date_time = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
	order_id = str(uuid.uuid1())
	
	snap = midtransclient.Snap(
		is_production=False,
		server_key=serverkey,
		client_key=clientkey
	)
	
	transaction = snap.create_transaction({
		"transaction_details": {
			"order_id": order_id,
			"gross_amount": amount
		}, "credit_card":{
			"secure" : True
		}
	})
	
	redirect_url = transaction['redirect_url']
	
	Donation.objects.create(name=name, email=email, phone=phone, amount=amount, 
	requestdatetime=date_time, orderid=order_id, redirecturl=redirect_url, 
	paymentgateway="MIDTRANS", trxstatus="PENDING")
	
	return {"data": redirect_url}
	
def handle_notify_midtrans(request):
	if request.method != "POST": return non_post_msg()
	
	order_id = request.POST['order_id']
	
	midtrans_env = get_midtrans_donation_param_api()
	merchantid = midtrans_env["MerchantID"]
	clientkey = midtrans_env["ClientKey"]
	serverkey = midtrans_env["ServerKey"]
	
	api_client = midtransclient.CoreApi(
		is_production=False,
		server_key=serverkey,
		client_key=clientkey
	)
	
	status_response = api_client.transactions.status(order_id)
	
	transaction_status = status_response['transaction_status']
	fraud_status = status_response['fraud_status']
	
	trxstatus = None
	
	if transaction_status == 'capture' or transaction_status == 'settlement':
		if fraud_status == 'challenge':
			trxstatus = 'CHALLENGE'
		elif fraud_status == 'accept':
			trxstatus = 'SUCCESS'
	elif transaction_status == 'cancel' or transaction_status == 'deny' or transaction_status == 'expire':
		trxstatus = 'FAILED'
	elif transaction_status == 'pending':
		trxstatus = 'PENDING'
	
	donation = Donation.objects.get(orderid=order_id)
	donation.trxstatus = trxstatus
	if trxstatus == "SUCCESS":
		name = donation.name
		description = "Donation from " + name + " with Payment Gateway MIDTRANS"
		date_time = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
			
		income = Income.objects.create(datestamp=date_time, description=description, amount=int(donation.amount))
			
		donation.incomeid = income
	donation.save()
	
	return {"data": trxstatus}

def non_post_msg():
	error_payload = {"success": False, "message": "Request must be POST"}
	return {"data": error_payload}